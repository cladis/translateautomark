<html>
    <head>
        <style>
            .marked {background-color: silver;}
            .unmarked-different {background-color: yellow;}
            .unmarked-same {background-color: greenyellow;}
        </style>
    </head>
    <body>
        <?php
        include_once ('botclasses.php');
        include_once ('basebot.php');
        $wiki = new basebot("https://meta.wikimedia.org/w/api.php");
        $wiki->setUserAgent('User-Agent: BaseBot (https://meta.wikimedia.org/wiki/User:BaseBot)');
        $wiki_mycnf = parse_ini_file("/data/project/basebot/wiki.my.cnf");
        $wiki->login($wiki_mycnf['user_mainacc'], $wiki_mycnf['password_mainacc']);

        $wiki->setLogin($login);
        $translatelog = $wiki->translatelog();
        $title_state = [];
        $action_types = [];

//Glossary: 
// mark      = $1 позначив $3 для перекладу 
// unmark    = $1 вилучив $3 з перекладу 
// deletelok = $1 здійснив вилучення сторінки-перекладу $3 
// deletefok = $1 здійснив вилучення перекладабельної сторінки $3 
// moveok    = $1 здійснив перейменування перекладабельної сторінки $3 на $4 
// prioritylanguages = $1 встановив пріоритетні мови для перекладабельної сторінки $3: $5 
// discourage = $1 заборонив переклад $3 
// encourage = $1 дозволив переклад $3 
// associate = $1 додав перекладабельну сторінку $3 до агрегованої групи $4 
// dissociate = $1 вилучив перекладабельну сторінку $3 з агрегованої групи $4 
// deletefnok = $1 не зміг вилучити $3, що належить до перекладабельної сторінки $4 
// deletelnok = $1 не зміг вилучити $3, що належить до сторінки-перекладу $4 
//
//echo '<table border=1>';
        foreach ($translatelog as $logentry) {
            //var_dump($logentry);
            $title_state[$logentry["pageid"]]["pagename"] = $logentry["title"]; // just for the sake of human readability
            $title_state[$logentry["pageid"]]["lastaction"] = $logentry["action"];
            $action_types[$logentry["action"]] = true;
            if ($title_state[$logentry["pageid"]]["lastaction"] == "mark") {
                $title_state[$logentry["pageid"]]["revision"] = $logentry["params"]["revision"];
                $title_state[$logentry["pageid"]]["evertranslatable"] = true;
            } elseif ($title_state[$logentry["pageid"]]["lastaction"] == "unmark" || !isset($title_state[$logentry["pageid"]]["revision"])) {
                // in case of unmark set -1. do not change in case of anything else
                // also set to -1 if there was nothing else before
                $title_state[$logentry["pageid"]]["revision"] = -1;
            }
            if (!isset($title_state[$logentry["pageid"]]["evertranslatable"])) {
                $title_state[$logentry["pageid"]]["evertranslatable"] = false;
            }
        }unset($logentry);
        $ts_mycnf = parse_ini_file("/data/project/basebot/replica.my.cnf");
// Create connection
        $metawiki_p = new mysqli("metawiki.labsdb", $ts_mycnf['user'], $ts_mycnf['password'], "metawiki_p");
// Check connection
        if ($metawiki_p->connect_error) {
            die("Connection to DB failed: " . $metawiki_p->connect_error);
        }
        echo 'connected to DB at the least<br/>';
        $sql = '
  SELECT
  rev_id,      -- revision from log ID
  rev_sha1,     
  page_id,     -- page id
  page_title,  -- 
  page_latest,
  CASE WHEN rev_id = page_latest THEN rev_sha1 ELSE
    (SELECT rev_sha1 FROM revision_userindex WHERE rev_id = page_latest) END AS latest_sha1

  FROM revision_userindex
  JOIN page ON rev_page = page_id
  WHERE rev_id IN (
    -1';

        foreach ($title_state as $state) {
            if ($state["revision"] == -1 || !evertranslatable || $state["lastaction"] == "discourage") {
                continue;
            }
            $sql .= ",\n    " . $state["revision"];
        }

        $sql .= "\n)\n";
        $result = $metawiki_p->query($sql);

        echo '<table border=1>' . PHP_EOL;
        echo '<tr>' . PHP_EOL;
        echo '<th>rev_id' . PHP_EOL;
        echo '<th>rev_sha1' . PHP_EOL;
        echo '<th>page_id' . PHP_EOL;
        echo '<th>page_title' . PHP_EOL;
        echo '<th>page_latest' . PHP_EOL;
        echo '<th>latest_sha1' . PHP_EOL;
        echo '<th>class' . PHP_EOL;
        echo '</tr>' . PHP_EOL;
        while ($row = $result->fetch_assoc()) {
            $class = "";
            if ($row["rev_id"] == $row["page_latest"]) {
                $class = "marked";
            } elseif (strcmp($row["rev_sha1"], $row["latest_sha1"]) == 0) {
                $class = "unmarked-same";
            } else {
                $class = "unmarked-different";
            }
            echo "<tr class='{$class}'>" . PHP_EOL;
            echo '<td>' . $row["rev_id"] . PHP_EOL;
            echo '<td>' . $row["rev_sha1"] . PHP_EOL;
            echo '<td>' . $row["page_id"] . PHP_EOL;
            echo '<td>' . $row["page_title"] . PHP_EOL;
            echo '<td>' . $row["page_latest"] . PHP_EOL;
            echo '<td>' . $row["latest_sha1"] . PHP_EOL;
            echo '<td>' . $class . PHP_EOL;
            echo '</tr>' . PHP_EOL;
        }
        echo '</table>' . PHP_EOL;
        $metawiki_p->close(); // we don't need the connection itself anymore
        ?>
    </body>
</html>