<?php

/**
 * Base's class extending Cris-G's botclasses
 *
 * @author Bohdan Melnychuk < base-w at yandex.ru >
 */
class basebot extends extended {

    /**
     * Returns an array with all the members of $category associated with
     * top revision users
     * @param $category The category to use.
     * @param $gcmtype (string) Can be file
     * @return array title => user
     * */
    function categorymembers_toprevusers($category, $gcmtype = "") {
        $continue = '';
        $pages = array();
        while (true) {
            $res = $this->query('?action=query&format=php&prop=revisions&generator=categorymembers&rvprop=user&gcmtitle=' . urlencode($category) . '&gcmtype=' . $gcmtype . '&gcmlimit=max' . $continue);
            if (isset($x['error'])) {//can it even work?
                return false;
            }
            foreach ($res['query']['pages'] as $x) {
                $pages[$x['title']] = $x['revisions'][0]['user'];
            }
            echo PHP_EOL . $res['continue']['gcmcontinue'] . PHP_EOL;
            if (empty($res['continue']['gcmcontinue'])) {
                return $pages;
            } else {
                $continue = '&gcmcontinue=' . urlencode($res['continue']['gcmcontinue']);
            }
        }
    }

    /**
     * Returns an array with all the members of $category associated with
     * top revision users
     * @param $category The category to use.
     * @param $gcmtype (string) Can be file
     * @return array title => user
     * */
    function categorymembers_imageinfo_and_description($category, $gcmlimit = 50) {
        $continue = '';
        $pages = array();
        while (true) {
///w/api.php?action=query&format=xml
//&prop=revisions%7Cimageinfo&generator=categorymembers&redirects=1&rvprop=timestamp%7Ccontent%7Cids
//&iiprop=timestamp%7Ccomment%7Cuser%7Cdimensions%7Cbitdepth%7Csize
//&gcmtitle=Category%3AImages_from_Wiki_Loves_Monuments_2017_in_Ukraine&gcmnamespace=&gcmtype=file&gcmlimit=500&gcmdir=ascending
            $res = $this->query(
                    '?action=query&format=php'
                    . '&prop=revisions|imageinfo'
                    . '&generator=categorymembers'
                    . '&redirects=1'
                    . '&rvprop=timestamp|content|ids'
                    . '&gcmtitle=' . urlencode($category)
                    . '&iiprop=timestamp|comment|user|dimensions|bitdepth|size'
                    . '&iilimit=max'
                    . '&gcmtype=file'
                    . '&gcmlimit=' . $gcmlimit
                    . $continue
            );
            if (isset($x['error'])) {//can it even work?
                return false;
            }
            foreach ($res['query']['pages'] as $x) {
                $pages[] = $x;
            }
            echo PHP_EOL . $res['continue']['gcmcontinue'] . PHP_EOL;
            if (empty($res['continue']['gcmcontinue'])) {
                return $pages;
            } else {
                $continue = '&gcmcontinue=' . urlencode($res['continue']['gcmcontinue']);
            }
        }
    }

    /**
     * Returns file info for a given file
     * @param type $f - file name, File: prefix included
     * @return array of file infos from newest revision to oldest
     * returns the maximum of 5000 for bots
     * the keys are:
     * timestamp, user, size, width, height, comment, bitdepth
     */
    function getfileinfo($f) {
        // /w/api.php?action=query&format=xml&prop=imageinfo&titles=%D0%A4%D0%B0%D0%B9%D0%BB%3ASchulte+vd+Luhe+wappen.png&iiprop=timestamp%7Ccomment%7Cuser%7Cdimensions%7Cbitdepth%7Csize&iilimit=max

        $res = $this->query('?action=query&format=php&prop=imageinfo&titles=' . urlencode($f) . '&iiprop=timestamp%7Ccomment%7Cuser%7Cdimensions%7Cbitdepth%7Csize&iilimit=max');
        foreach ($res['query']['pages'] as $page) {
            return $page['imageinfo'];
        }
    }
/**
 * 
 * @return array of logevents
 * 
 */
    function translatelog() {
        $continue = '';
        $logevents = array();
        while (true) {
//meta.wikimedia.org/w/api.php?action=query&format=xml&list=logevents&letype=pagetranslation&lelimit=max
            $res = $this->query(
                    '?action=query&format=php'
                    . '&list=logevents'
                    . '&letype=pagetranslation'
                    . '&lelimit=max'
                    . '&ledir=newer'
                    . $continue
            );
            if (isset($x['error'])) {//can it even work?
                return false;
            }
            foreach ($res['query']['logevents'] as $x) {
                $logevents[] = $x;
            }
            echo PHP_EOL . $res['continue']['lecontinue'] . PHP_EOL;
            if (empty($res['continue']['lecontinue'])) {
                return $logevents;
            } else {
                $continue = '&lecontinue=' . urlencode($res['continue']['lecontinue']);
            }
        }
    }

    /**
     * Uploads an image.
     * @param $page The destination file name.
     * @param $file The local file path.
     * @param $desc The upload description (defaults to '').
     * */
    function upload_nofile($page, $file, $desc = '') {
        if (!isset($file)) {
            return array('errors' => array(
                    "No file provided!"));
        }
        if ($this->token == null) {
            $this->token = $this->getedittoken();
        }

        $params = array(
            'filename' => $page,
            'comment' => $desc,
            'text' => $desc,
            'token' => $this->token,
            'ignorewarnings' => '1',
            'file' => new CURLFile($file)
        );
        return $this->query('?action=upload&format=php', $params);
    }

}
